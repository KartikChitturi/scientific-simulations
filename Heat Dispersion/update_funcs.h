#ifndef _update_funcs__
#define _update_funcs__

struct temp{
	double t;
	int h;
};

void update(int size_x,int size_y,double alpha,struct temp **grid,struct temp **updateGrid);

#endif
