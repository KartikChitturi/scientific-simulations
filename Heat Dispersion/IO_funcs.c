#include<stdio.h>
#include<string.h>
#include "IO_funcs.h"

void readFile(FILE *inputFile, int size_x,int size_y,struct temp **grid){

int i,j;
int l,m;
char tempx[30];
char tempy[30];
double ttemp;
int thold;


	while(!feof(inputFile)){
    fscanf(inputFile,"%s %s %lf %d",tempx,tempy,&ttemp,&thold);
		if(strcmp(tempx,"*")==0 && strcmp(tempy,"*")==0){
			for(l=0;l<size_x;l++){
				for(m=0;m<size_y;m++){
					grid[l][m].t=ttemp;
					grid[l][m].h=thold;
				}
			}
		}
		else if(strcmp(tempx,"*")==0){
			for(l=0;l<size_x;l++){
				grid[l][atoi(tempy)].t=ttemp;
				grid[l][atoi(tempy)].h=thold;
			}
		}
		else if(strcmp(tempy,"*")==0){
			for(m=0;m<size_y;m++){
				grid[atoi(tempx)][m].t=ttemp;
				grid[atoi(tempx)][m].h=thold;
			}
		}
		else{
    grid[atoi(tempx)][atoi(tempy)].t=ttemp;
    grid[atoi(tempx)][atoi(tempy)].h=thold;
		}
  }
}


void writeFile(int size_x, int size_y, int t, int outputFreq, char *reslen, char *resname, char *outputBase, FILE *resFile, struct temp **grid){

int i,j;

if(t%outputFreq==0){
  sprintf(reslen,"_%04d.dat",t);
  strcpy(resname,outputBase);
  strcat(resname,reslen);
  resFile=fopen(resname,"w");
  for(i=0;i<size_x;i++){
    for(j=0;j<size_y;j++){
      fprintf(resFile,"%d %d: %lf\n",i,j,grid[i][j].t);
    }
  }
}
}
