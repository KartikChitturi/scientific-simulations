#!/bin/bash


#Issue a regular shell command
ls *

#Loop over datafiles and call gnuplot
mkdir -p figures
for i in `ls *_*.dat`
do
  gnuplot -e "filename='$i'" -e "outfile='figures/$i.png'" sim.gplot
done

