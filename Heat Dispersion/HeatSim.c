#include<stdio.h>
#include<stdlib.h>
#include "update_funcs.h"
#include "IO_funcs.h"

int main(int argc, char **argv){

char inputName[30];
int outputFreq;
char outputBase[30];
char resname[30];
char reslen[30];

int size_x,size_y;
double alpha;
int num_steps;

int i,j,t;
int tempx,tempy;
double ttemp;
int thold;
/*
struct temp{
	double t;
	int h;
};
*/
struct temp **grid;
struct temp **updateGrid;


strcpy(inputName,argv[1]);
outputFreq=atoi(argv[2]);
strcpy(outputBase,argv[3]);

FILE *inputFile;
inputFile=fopen(inputName,"r");
fscanf(inputFile,"%d %d %lf %d",&size_x,&size_y,&alpha,&num_steps);

grid=(struct temp**)malloc(sizeof(struct temp*)*size_x);
for(j=0;j<size_y;j++){
	grid[j]=(struct temp*)malloc(sizeof(struct temp)*size_y);
}


updateGrid=(struct temp**)malloc(sizeof(struct temp*)*size_x);
for(j=0;j<size_y;j++){
  updateGrid[j]=(struct temp*)malloc(sizeof(struct temp)*size_y);
}


readFile(inputFile,size_x,size_y,grid);

fclose(inputFile);

FILE *resFile;

for(t=1;t<num_steps+1;t++){
update(size_x,size_y,alpha,grid,updateGrid);
writeFile(size_x, size_y,t, outputFreq, reslen, resname, outputBase, resFile,grid);
}

for(i=0;i<size_x;i++){
	free(grid[i]);
}
free(grid);


for(i=0;i<size_x;i++){
  free(updateGrid[i]);
}
free(updateGrid);

return 0;
}
