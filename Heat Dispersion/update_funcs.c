#include<stdio.h>
#include<stdlib.h>
#include "update_funcs.h"

void update(int size_x,int size_y,double alpha,struct temp **grid,struct temp **updateGrid){

int i,j;


for(i=0;i<size_x;i++){
  for(j=0;j<size_y;j++){
	if(grid[i][j].h==0){
		//Corners
    if(i==0 && j==0){
    updateGrid[i][j].t=grid[i][j].t+(alpha)*(grid[i+1][j].t+grid[i][j+1].t+grid[i+1][j+1].t-3*grid[i][j].t);
    }
    else if(i==0 && j==size_y-1){
    updateGrid[i][j].t=grid[i][j].t+(alpha)*(grid[i+1][j].t+grid[i][j-1].t+grid[i+1][j-1].t-3*grid[i][j].t);
    }
    else if(i==size_x-1 && j==size_y-1){
    updateGrid[i][j].t=grid[i][j].t+(alpha)*(grid[i-1][j].t+grid[i][j-1].t+grid[i-1][j-1].t-3*grid[i][j].t);
    }
    else if(i==size_x-1 && j==0){
    updateGrid[i][j].t=grid[i][j].t+(alpha)*(grid[i-1][j].t+grid[i][j+1].t+grid[i-1][j+1].t-3*grid[i][j].t);
    }

		//Edges
    else if(i==0){
    updateGrid[i][j].t=grid[i][j].t+(alpha)*(grid[i][j-1].t+grid[i][j+1].t+grid[i+1][j-1].t+grid[i+1][j].t+grid[i+1][j+1].t-5*grid[i][j].t);
    }
    else if(j==0){
    updateGrid[i][j].t=grid[i][j].t+(alpha)*(grid[i-1][j].t+grid[i+1][j].t+grid[i-1][j+1].t+grid[i][j+1].t+grid[i+1][j+1].t-5*grid[i][j].t);
    }
    else if(i==size_x-1){
    updateGrid[i][j].t=grid[i][j].t+(alpha)*(grid[i][j-1].t+grid[i][j+1].t+grid[i-1][j-1].t+grid[i-1][j].t+grid[i-1][j+1].t-5*grid[i][j].t);
    }
    else if(j==size_y-1){
    updateGrid[i][j].t=grid[i][j].t+(alpha)*(grid[i-1][j].t+grid[i+1][j].t+grid[i-1][j-1].t+grid[i][j-1].t+grid[i+1][j-1].t-5*grid[i][j].t);
    }
	//Interior
  else{
    updateGrid[i][j].t=grid[i][j].t+(alpha)*(grid[i-1][j-1].t+grid[i][j-1].t+grid[i+1][j-1].t+grid[i-1][j].t+grid[i+1][j].t+grid[i-1][j+1].t+grid[i][j+1].t+grid[i+1][j+1].t-8*grid[i][j].t);
  }
	}
	else{
		updateGrid[i][j].t=grid[i][j].t;
	}
  }
}

for(i=0;i<size_x;i++){
  for(j=0;j<size_y;j++){
    grid[i][j].t=updateGrid[i][j].t;
  }
}

}
