#ifndef _IO_funcs__
#define _IO_funcs__

#include "update_funcs.h"

void readFile(FILE *inputFile, int size_x,int size_y,struct temp **grid);
void writeFile(int size_x, int size_y, int t, int outputFreq, char *reslen, char *resname, char *outputBase, FILE *resFile, struct temp **grid);

#endif
